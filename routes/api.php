<?php

use Illuminate\Http\Request;

// Show a particular image
Route::get('/image/show/{name}', 'ImageController@show');

Route::group(['middleware' => 'auth:api'], function () {
	// Search users
	Route::post('/user/search', 'User\SearchController@run');
	// Create a new user.
	Route::post('/user/{id}/edit', 'User\UpdateController@update');
	// Upload an image
	Route::post('/image/upload', 'ImageController@upload');
});

Route::group(['middleware' => array('auth:api', 'role:teacher')], function () {
	// Create a new user.
	Route::post('/user/create', 'User\CreateController@generate');
	// Create a new group.
	Route::post('/group/create', 'GroupController@create');
	// Delete the group
	Route::post('/group/{id}/delete', 'GroupController@delete');
	// Create a new subject.
	Route::post('/subject/create', 'SubjectController@create');
	// Delete the subject
	Route::post('/subject/{id}/delete', 'SubjectController@delete');
	// Update a user progress.
	Route::post('/user/{id}/progress/update', 'User\ProgressController@update');
	// Select a group progress.
	Route::post('/diary/{group}', '\App\Http\Controllers\DiaryController@select');
	// Select a group statement.
	Route::post('/statement', '\App\Http\Controllers\StatementController@select');
});