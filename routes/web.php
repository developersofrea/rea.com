<?php

use Illuminate\Http\Request;

Route::group(['middleware' => array('auth', 'role:teacher')], function () {
	// The user create form
	Route::get('/user/create', 'UserController@showCreateForm');
	// The user progress manager form
	Route::get('/user/{user}/progress', 'UserController@showManagerForm');
	// The groups manager form
	Route::get('/group/manager', 'GroupController@showManagerForm');
	// The subjects manager form
	Route::get('/subject/manager', 'SubjectController@showManagerForm');
	// The diary page
	Route::get('/main/diary', 'DiaryController@show');
	// The statement page
	Route::get('/main/statement', 'StatementController@show');
});

Route::group(['middleware' => 'auth'], function () {
	// The welcome page
	Route::get('/', function () {
	    return view('welcome');
	});
	// The users search page
	Route::get('/user/search', 'UserController@showSearchForm');
	// The page of a user
	Route::get('/user/{user}', 'UserController@showProfile');
	// The user edit form
	Route::get('/user/{user}/edit', 'UserController@showEditForm');
	// Logout
	Route::get('/auth/logout', 'Auth\LoginController@logout');

	Route::get('/chat/{chat}', 'Chat\ChatController@showChatForm');
});

Route::group(['middleware' => 'guest'], function () {
	// Login form
	Route::get('/auth/login', 'Auth\LoginController@showLoginForm');
	// Login handler
	Route::post('/auth/login', 'Auth\LoginController@login');
});