<?php
namespace App\Modules\Image;

use Image as ImageHelper;

class Helper
{
    protected static $image = null;
    protected static $user_id = null;
    protected static $allowed_file_size = 1048576;
    protected static $allowed_files = ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg'];

    public static function make($image)
    {
        self::$image = self::getImage($image);
        return new static;
    }

    public static function upload($user_id = null, $path = 'photos/', $format = 'jpg')
    {
        self::$user_id = $user_id;
        $name = self::getMd5($user_id);
        $path = $path . $name . '.' . $format;
        $image_saved = self::$image->save(storage_path($path));
        return (object) [
            'name' => $name,
            'path' => $path,
            'user_id' => $user_id
        ];
    }

    public static function validate()
    {
        if(!self::checkSize(self::$image->filesize())) {
            return 'Пожалуйста, убедитесь, что изображение меньше, чем 1 MB.';
        }
        if(self::checkMime(self::$image->mime())) {
            return 'Данный тип файла не поддерживается.';
        }
        return false;
    }

    private static function getImage($image)
    {
        return is_a($image, 'ImageHelper') ? $image: ImageHelper::make($image);
    }

    private static function getMd5($salt)
    {
        return md5( strval($salt) . str_random(20) . strval(\time()));
    }

    private static function checkSize($size)
    {
        return !($size >= self::$allowed_file_size);
    }

    private static function checkMime($file_mime)
    {
        return array_key_exists($file_mime, self::$allowed_files);
    }
}
