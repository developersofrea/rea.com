<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progress extends Model
{
	protected $table = 'progresses';

	protected $fillable = [
        '1_sem', '2_sem', '3_sem', '4_sem', '5_sem', '6_sem', '7_sem', '8_sem', 'grade', 'subject_id', 'student_id', 'date'
    ];

    public function subject()
    {
    	return $this->hasOne('App\Subject', 'id', 'subject_id');
    }
}
