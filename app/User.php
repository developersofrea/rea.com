<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'first_name', 'last_name', 'middle_name', 'type', 'phone',
        'residence', 'place_of_work', 'birthday', 'gender', 'group_id', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($role)
    {
        return $this->type == $role;
    }

    public function group()
    {
        return $this->hasOne('App\Group', 'id', 'group_id');
    }

    public function progress()
    {
        return $this->hasMany('App\Progress', 'student_id', 'id');
    }
}
