<?php

namespace App\Chat;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = "chats";

    protected $fillable = [
        'name', 'type'
    ];
}
