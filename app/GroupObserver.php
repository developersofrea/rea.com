<?php

namespace App;

use App\Group;
use App\Chat\Chat;

class GroupObserver
{

	public function creating(Group $group)
	{
		return $group->chat_id = $this->createChat($group)->id;
	}

	public function createChat(Group $group)
	{
		return Chat::create([
				'type' => 'group',
				'name' => $group->name
			]);
	}
}