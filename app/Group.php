<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'name', 'start_date'
    ];

    public function users()
    {
    	return $this->hasMany('App\User', 'group_id', 'id')->select('avatar', 'first_name', 'last_name', 'middle_name', 'id', 'group_id');
    }

    public function chat()
    {
    	return $this->belongsTo('App\Chat\Chat');
    }
}
