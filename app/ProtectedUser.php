<?php

namespace App;

use App\User;

class ProtectedUser extends User
{
    protected $fillable = [
        'username', 'email', 'password', 'avatar'
    ];
}
