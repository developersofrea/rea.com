<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To create a new user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::create([
                'username' => $this->ask('Login'),
                'password' => Hash::make($this->ask('Password')),
                'email' => $this->ask('email'),
                'first_name' => $this->ask('First name'),
                'last_name' => $this->ask('Last name'),
                'middle_name' => $this->ask('Middle name'),
                'type' => $this->anticipate('User type', [ 'студент', 'преподаватель' ]),
                'gender' => $this->ask('Gender (1 - male, 2 - female)')
            ]);
        $this->info('The user created');
    }
}
