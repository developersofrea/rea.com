<?php

namespace App\Http\Controllers\API\v1;

use Validator;
use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\v1\Response;

class SubjectController extends Controller
{
    use Response;

    public function create(Request $request)
    {
    	$validator = $this->validator($request->all());
    	if($validator->fails()) {
    		return $this->sendErrorResponse($validator->errors());
    	}
    	return $this->sendSuccessResponse(Subject::create($request->all()));
    }

    public function delete(Subject $group, $id)
    {
    	if($group->destroy($id)) {
    		return $this->sendSuccessResponse();
    	}
    	return $this->sendErrorResponse('Что-то пошло не так!');
    }

    protected function validator(array $data)
    {
    	return Validator::make($data, [
    			'name' => 'required|max:255|unique:groups'
    		]);
    }
}
