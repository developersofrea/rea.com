<?php

namespace App\Http\Controllers\API\v1;

/**
 * The trait makes easy to response api's data.
 */
trait Response
{

    public function sendSuccessResponse($response = null)
    {
        return $this->sendResponse(true, $response);
    }

    public function sendErrorResponse($response = null)
    {
        return $this->sendResponse(false, $response);
    }

    private function sendResponse($success, $response = null)
    {
        return response()->json([
            'success' => $success,
            'data' => $response
        ]);
    }
}
