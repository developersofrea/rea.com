<?php

namespace App\Http\Controllers\API\v1\User;

use Validator;
use App\UserSearch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\v1\Response;

class SearchController extends Controller
{
	use Response;

    public function run(Request $request, UserSearch $search)
    {
    	$validator = $this->validator(collect($request->all())->filter()->all());
    	if($validator->fails()) {
    		return $this->sendErrorResponse($validator->errors());
    	}
    	return $this->sendSuccessResponse($search->get(8));
    }

    protected function validator(array $data)
    {
    	return Validator::make($data, [
    			'request' => 'max:255',
    			'city' => 'max:255',
    			'gender' => 'numeric',
    			'age_from' => 'numeric',
    			'age_to' => 'numeric'
    		]);
    }
}
