<?php

namespace App\Http\Controllers\API\v1\User;

use Auth;
use Validator;
use App\User;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\v1\Response;
use App\Http\Controllers\API\v1\User\ImageTrait;

class CreateController extends Controller
{
	use Response, ImageTrait;

    public function generate(Request $request)
    {
    	$validator = $this->validator($request->all());
    	if($validator->fails()) {
    		return $this->sendErrorResponse($validator->errors());
    	}
    	if($request->has('image')) {
            $request->merge(array('avatar' => '/api/v1/image/show/' . $this->cropImage((object) $request->image)->name));
        }
    	return $this->sendSuccessResponse($this->create($request->except('image')));
    }

    protected function create(array $user)
    {
    	return User::create(array_set($user, 'password', Hash::make($user['password'])));
    }

    protected function cropImage($source)
    {
    	return Photo::create((array) $this->crop($source));
    }

    protected function validator(array $data)
    {
    	return Validator::make($data, [
    			'username' => 'required|max:255|unique:users',
    			'password' => 'required|min:6|confirmed',
    			'first_name' => 'required|max:255',
    			'last_name' => 'required|max:255',
    			'middle_name' => 'required|max:255',
    			'type' => 'required|max:255',
    			'gender' => 'required|integer',
    			'phone' => 'required|max:12',
    			'email' => 'required|email|max:255|unique:users',
    			'birthday' => 'required|date',
    			'group_id' => 'required|integer',
    		]);
    }
}
