<?php 
namespace App\Http\Controllers\API\v1\User;

use Auth;
use Image;
use App\Modules\Image\Helper;

trait ImageTrait
{

	public function crop($source)
	{
		$path = 'photos/' . $source->name . '.jpg';
		$image = Image::make(storage_path($path));
		$image = $image->crop(round($source->w), round($source->h), round($source->x), round($source->y));
		return Helper::make($image)->upload(Auth::id());
	}

}