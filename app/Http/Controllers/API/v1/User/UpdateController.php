<?php

namespace App\Http\Controllers\API\v1\User;

use Auth;
use App\User;
use App\Photo;
use Validator;
use App\ProtectedUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\v1\Response;
use App\Http\Controllers\API\v1\User\ImageTrait;

class UpdateController extends Controller
{
	use Response, ImageTrait;

    private $id;

    public function update ($id, Request $request)
    {
        $this->id = $id;
    	if(!isset($id) || $id == 'undefined' || $request->id != Auth::id() && !Auth::user()->hasRole('teacher')) {
    		return $this->sendErrorResponse('Этот аккаунт не принадлежит Вам.');
    	}

    	$validator = $this->validator($request->all());
    	if($validator->fails()) {
    		return $this->sendErrorResponse($validator->errors());
    	}

    	$this->involveImage($request);

    	return $this->sendSuccessResponse($this->accomplish($request->except('image')));
    }

    protected function involveImage($request)
    {
        if($request->has('image') && !empty(((object) $request->image)->url)) {
            $request->merge(array('avatar' => '/api/v1/image/show/' . $this->cropImage((object) $request->image)->name));
        }
    }

    protected function accomplish(array $user)
    {
        return $this->getModel()->update($this->takeReadyData($user));
    }

    protected function takeReadyData(array $user)
    {
        return isset($user['password']) ? array_set($user, 'password', Hash::make($user['password'])) : $user;
    }

    protected function getModel()
    {
        return Auth::user()->hasRole('teacher')? User::find($this->id): ProtectedUser::find($this->id);
    }

    protected function cropImage($source)
    {
    	return Photo::create((array) $this->crop($source));
    }

    protected function validator(array $data)
    {
    	return Validator::make($data, [
    			'username' => 'required|max:255|unique:users,username,' . $this->id,
    			'password' => 'min:6|confirmed',
    			'first_name' => 'required|max:255',
    			'last_name' => 'required|max:255',
    			'middle_name' => 'required|max:255',
    			'type' => 'required|max:255',
    			'gender' => 'required|integer',
    			'phone' => 'required|max:12',
    			'email' => 'required|email|max:255|unique:users,email,' . $this->id,
    			'birthday' => 'required|date',
    			'group_id' => 'required|integer',
    		]);
    }
}
