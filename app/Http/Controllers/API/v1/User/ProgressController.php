<?php

namespace App\Http\Controllers\API\v1\User;

use App\Progress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\v1\Response;

class ProgressController extends Controller
{
	use Response;

	private $user_id;

    public function update($id, Request $request)
    {
    	$this->user_id = $id;
    	foreach($request->table as $column) {
    		isset($column['id']) ? $this->amend($column): $this->create($column);
    	}
    	return $this->sendSuccessResponse();
    }

    private function amend($column)
    {
    	return Progress::where('id', $column['id'])->update($this->prepareArray($column));
    }

    private function create($column)
    {
    	return Progress::create($this->prepareArray($column));
    }

	private function prepareArray($array)
    {
    	return array_add(array_except($array, ['id', 'created_at', 'updated_at']), 'student_id', $this->user_id);
    }    
}
