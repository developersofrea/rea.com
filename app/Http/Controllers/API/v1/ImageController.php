<?php

namespace App\Http\Controllers\API\v1;

use Image;
use App\Photo;
use App\Modules\Image\Helper;
use App\Http\Controllers\API\v1\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
	use Response; 

    public function show($name)
    {
        return Image::make(storage_path('photos/' . $name . '.jpg'))->response();
    }

    public function upload(Request $request)
    {
    	$image = Helper::make($request->image);
    	if($validator = $image->validate()) {
            return $this->sendErrorResponse($validator);
    	}
    	$image = $image->upload(\Auth::user()->id);
    	$create = $this->create($image);

        return (!$image || !$create) ? 
        		$this->sendErrorResponse('Что-то пошло не так!') :
        		$this->sendSuccessResponse($this->getHandler($create));
    }

    private function getHandler($image)
    {
        return [
            'message' => 'Изображение успешно загружено на сервер.',
            'image' => [
                'name' => $image->name,
                'id' => $image->id,
                'user_id' => $image->user_id,
                'url' => '/api/v1/image/show/' . $image->name
            ]
        ];
    }

    private function create($image)
    {
        return Photo::create((array)$image);
    }
}
