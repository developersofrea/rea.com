<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function showManagerForm(Request $request)
    {
    	return view('main.pages.subject.manager', [
    			'subjects' => Subject::all()
    		]);
    }
}
