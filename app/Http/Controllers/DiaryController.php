<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\API\v1\Response;

class DiaryController extends Controller
{
	use Response;

    public function show(Request $request)
    {
    	return view('main.pages.diary', [
    			'group' => $request->input('group', null),
    			'groups' => Group::all()
    		]);
    }

    public function select(Group $group, Request $request)
    {
    	$result = $group->load('users', 'users.progress', 'users.progress.subject');
    	if(! count($result->users)) {
    		return $this->sendErrorResponse();
    	}
    	return $this->sendSuccessResponse([
    			'result' => $result,
    			'subjects' => $result->users->pluck('progress.*.subject.name')->flatten()->unique()
    		]);
    }
}
