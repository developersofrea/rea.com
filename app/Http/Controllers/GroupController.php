<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function showManagerForm(Request $request)
    {
    	return view('main.pages.group.manager', [
    			'groups' => Group::all()
    		]);
    }
}
