<?php

namespace App\Http\Controllers\Chat;

use App\Chat\Chat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{
    public function showChatForm(Chat $chat)
    {
    	return view('main.chat.chat', [
    			'chat' => $chat
    		]);
    }
}
