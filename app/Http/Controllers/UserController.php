<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use App\Subject;
use App\UserSearch;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function showCreateForm()
    {
    	return view('main.pages.user.create', [
    			'groups' => Group::all()
    		]);
    }

    public function showEditForm(User $user)
    {
        return view('main.pages.user.edit', [
                'user' => $user,
                'groups' => Group::all()
            ]);
    }

    public function showProfile(User $user)
    {
    	return view('main.pages.user.profile', [
    			'profile' => $user->load('group', 'progress', 'progress.subject')
    		]);
    }

    public function showManagerForm(User $user, Request $request)
    {
        return view('main.pages.user.progress', [
                'profile' => $user->load('group', 'progress'),
                'subjects' => Subject::all()
            ]);
    }

    public function showSearchForm(Request $request, UserSearch $search)
    {
        return view('main.pages.search', [
                'request' => $request->input('request', ''),
                'city' => $request->input('city', ''),
                'gender' => $request->input('gender', ''),
                'age_from' => $request->input('age_from', ''),
                'age_to' => $request->input('age_to', ''),
                'database' => $search->get(8)
            ]);
    }
}
