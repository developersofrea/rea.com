<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\API\v1\Response;

class StatementController extends Controller
{
	use Response;

    public function show()
    {
    	return view('main.pages.statement', [
    			'groups' => Group::all(),
    			'subjects' => Subject::all()
    		]);
    }

    public function select(Request $request)
    {
    	$users = User::with(['progress' => function ($query) use ($request) {
    		return $query->where('subject_id', $request->subject);
    	}])->where('group_id', $request->group)->get();
    	return $this->sendSuccessResponse($users);
    }
}
