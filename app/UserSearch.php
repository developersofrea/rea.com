<?php 

namespace App;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class UserSearch
{
	private $request;

	function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function get($pages = 15)
	{
		$users = $this->select();
		return $this->filters($users)->paginate($pages);
	}

	protected function select()
	{
		return User::select('id', 'first_name', 'last_name', 'middle_name', 'avatar');
	}

    protected function request(Builder $query, $value)
    {
    	return $query->where(DB::raw("concat(first_name, ' ', last_name, ' ', middle_name)"), 'like', '%' . $value . '%');
    }

    protected function city(Builder $query, $value)
    {
    	return $query->where('residence', 'like', '%' . $value . '%');
    }

    protected function gender(Builder $query, $value)
    {
    	return $query->where('gender', '=', $value);
    }

    protected function ageFrom(Builder $query, $value)
    {
    	return $query->where(DB::raw("TIMESTAMPDIFF(YEAR, birthday, CURDATE())"), '>=', $value);
    }

    protected function ageTo(Builder $query, $value)
    {
    	return $query->where(DB::raw("TIMESTAMPDIFF(YEAR, birthday, CURDATE())"), '<=', $value);
    }

    protected function filters(Builder $query)
    {
        foreach (collect($this->request->all())->filter()->all() as $key => $value) {
            $query = $this->callFilter($query, camel_case($key), $value);
        }
        return $query;
    }

    protected function callFilter(Builder $query, $filter, $value)
    {
        return method_exists($this, $filter) ? $this->{$filter}($query, $value) : $query;
    }
}