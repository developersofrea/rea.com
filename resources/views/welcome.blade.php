@extends('main.layouts.base')

@section('title', 'Главная страница')

@section('content')
    <p class="header message">@lang('messages.welcome', [ 'name' => Auth::user()->first_name ])</p>
    <div class="columns">
        @if(Auth::user()->hasRole('teacher'))
        <a class="column col-4 col-sm-12" href="/user/create">
            <div class="action action-green">
                <i class="fa fa-plus-circle fa-3x" aria-hidden="true"></i>
                <p>@lang('menu.user.create')</p>
            </div>
        </a>
        @endif
        @if(Auth::user()->hasRole('teacher'))
        <a class="column col-4 col-sm-12" href="/group/manager">
            <div class="action action-orange">
                <i class="fa fa-users fa-3x" aria-hidden="true"></i>
                <p>Менеджер групп</p>
            </div>
        </a>
        @endif
        @if(Auth::user()->hasRole('teacher'))
        <a class="column col-4 col-sm-12" href="/subject/manager">
            <div class="action" style="background-color: #daa7b6">
                <i class="fa fa-list-alt fa-3x" aria-hidden="true"></i>
                <p>Менеджер предметов</p>
            </div>
        </a>
        @endif
        @if(Auth::user()->hasRole('teacher'))
        <a class="column col-4 col-sm-12" href="/main/diary">
            <div class="action action-pink">
                <i class="fa fa-book fa-3x" aria-hidden="true"></i>
                <p>@lang('menu.diary')</p>
            </div>
        </a>
        @endif
        @if(Auth::user()->hasRole('teacher'))
        <a class="column col-4 col-sm-12" href="/main/statement">
            <div class="action" style="background-color: rgb(166, 193, 231)">
                <i class="fa fa-address-book fa-3x" aria-hidden="true"></i>
                <p>Электронная ведомость</p>
            </div>
        </a>
        @endif
        <a class="column col-4 col-sm-12" href="/user/search">
            <div class="action action-blue">
                <i class="fa fa-search fa-3x" aria-hidden="true"></i>
                <p>@lang('menu.users.search')</p>
            </div>
        </a>
        <a class="column col-4 col-sm-12" href="/user/{{ Auth::id() }}">
            <div class="action action-purple">
                <i class="fa fa-user fa-3x" aria-hidden="true"></i>
                <p>@lang('menu.user.mine')</p>
            </div>
        </a>
        <a class="column col-4 col-sm-12" href="/user/{{ Auth::id() }}/edit">
            <div class="action action-lavender">
                <i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></i>
                <p>@lang('menu.user.edit.mine')</p>
            </div>
        </a>
        <a class="column col-4 col-sm-12" href="/auth/logout">
            <div class="action action-red">
                <i class="fa fa-sign-out fa-3x" aria-hidden="true"></i>
                <p>@lang('menu.auth.logout')</p>
            </div>
        </a>
    </div>
@endsection