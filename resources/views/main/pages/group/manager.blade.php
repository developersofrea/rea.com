@extends('main.layouts.base')

@section('title', 'Менеджер групп')

@section('content')
    <group-manager :groups="{{ $groups }}" inline-template>
        <div>
            <div class="columns float-right">
                <div class="column col-8">
                    <input class="form-input" type="text" id="input-group" placeholder="Название группы..." v-model="form.name.value" />
                </div>
                <div class="column col-4">
                    <button class="btn btn-primary" :class="{ loading: request.loading }" @click="create">Добавить</button>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Название группы</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="group in data">
                        <td>@{{ group.id }}</td>
                        <td>@{{ group.name }}</td>
                        <td align="center">
                            <button class="btn btn-primary" @click="remove($event, group.id)">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </group-manager>
@endsection