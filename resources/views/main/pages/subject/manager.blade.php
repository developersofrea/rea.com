@extends('main.layouts.base')

@section('title', 'Менеджер предметов')

@section('content')
    <subject-manager :subjects="{{ $subjects }}" inline-template>
        <div>
            <div class="columns float-right">
                <div class="column col-8">
                    <input class="form-input" type="text" placeholder="Название предмета..." v-model="form.name.value" />
                </div>
                <div class="column col-4">
                    <button class="btn btn-primary" :class="{ loading: request.loading }" @click="create">Добавить</button>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Название предмета</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="subject in data">
                        <td>@{{ subject.id }}</td>
                        <td>@{{ subject.name }}</td>
                        <td align="center">
                            <button class="btn btn-primary" @click="remove($event, subject.id)">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </subject-manager>
@endsection