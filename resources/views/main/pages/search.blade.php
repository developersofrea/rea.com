@extends('main.layouts.base')

@section('title', 'Поиск пользователя')

@section('content')
    <page-search :request="'{{ $request }}'" :city="'{{ $city }}'" :gender="'{{ $gender }}'" :age_from="'{{ $age_from }}'" :age_to="'{{ $age_to }}'" :database="{{ $database->toJson() }}" inline-template>
        <div class="search">
            <!-- SEARCH -->
            <div class="columns">
                <div class="column col-9">
                    <div class="form-group">
                        <input class="form-input input-lg" type="text" placeholder="Поиск..." v-model="form.request.value" />
                    </div>
                </div>
                <div class="column col-3">
                    <button class="btn btn-primary btn-lg btn-block" :class="{ loading: query.loading }" @click="search">Найти</button>
                </div>
            </div>
            <!-- FILTERS -->
            <div class="columns">
                <div class="column col-5">
                    <div class="form-group">
                        <label class="form-label" for="input-city">Город</label>
                        <input class="form-input" type="text" id="input-city" placeholder="Москва" v-model="form.city.value" />
                    </div>
                </div>
                <div class="column col-3">
                    <div class="form-group">
                        <label class="form-label">Пол</label>
                        <label class="form-radio">
                            <input type="radio" name="gender" v-model="form.gender.value" value="1" />
                            <i class="form-icon"></i> Муж.
                        </label>
                        <label class="form-radio">
                            <input type="radio" name="gender" v-model="form.gender.value" value="2" />
                            <i class="form-icon"></i> Жен.
                        </label>
                    </div>
                </div>
                <div class="column col-4">
                    <label class="form-label">Возраст</label>
                    <div class="columns col-gapless">
                        <div class="column col-5" style="padding: 0">
                            <input class="form-input" type="number" id="input-city" placeholder="От:" min="1" v-model="form.age_from.value" />
                        </div>
                        <div class="column col-2" style="padding: 0">
                            <label class="text-center form-label label-lg">-</label>
                        </div>
                        <div class="column col-5" style="padding: 0">
                            <input class="form-input" type="number" id="input-city" placeholder="До:" min="1" v-model="form.age_to.value" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <!-- RESULTS -->
            <div class="columns">
                <div class="column col-3" v-for="user in users.data">
                    <a class="profile" :href="'/user/' + user.id">
                        <div class="image">
                            <img class="circle img-responsive" :src="user.avatar">
                        </div>
                        @{{ user.first_name }}</br>@{{ user.last_name }}
                    </a>
                </div>
            </div>
            <ul class="pagination float-right">
                <li class="page-item">
                    <a href="#" @click="prevPage" :disabled="users.current_page == 1">Предыдущая</a>
                </li>
                <li class="page-item" v-for="page in users.last_page" :class="{ active: page == users.current_page }" @click="openPage(page)">
                    <a href="#">@{{ page }}</a>
                </li>
                <li class="page-item">
                    <a href="#" @click="nextPage" :disabled="users.current_page == users.last_page">Следующая</a>
                </li>
            </ul>
        </div>
    </page-search>
@endsection