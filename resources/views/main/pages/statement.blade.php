@extends('main.layouts.base')

@section('title', 'Электронная ведомость')

@section('content')
    <page-statement inline-template>
        <div>
            <div class="columns">
                <div class="column col-6"></div>
                <div class="column col-3">
                    <div class="form-group">
                        <label class="form-label">Группа</label>
                        <select class="form-select" v-model="search.group">
                            <option value="" disabled>Выберите группу</option>
                            @foreach($groups as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="column col-3">
                    <div class="form-group">
                        <label class="form-label">Дисциплина</label>
                        <select class="form-select" v-model="search.subject">
                            <option value="" disabled>Выберите дисциплину</option>
                            @foreach($subjects as $subject)
                            <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="divider pt-10"></div>
            <table class="table table-hover table-header-rotated" :class="{ loading }" v-if="data">
                <thead>
                    <tr>
                        <th></th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Отчество</th>
                        <th class="active" align="center">Оценка</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="user in data">
                        <td align="center">
                            <figure class="avatar avatar-sm">
                                <img :src="user.avatar" />
                            </figure>
                        </td>
                        <td>@{{ user.last_name }}</td>
                        <td>@{{ user.first_name }}</td>
                        <td>@{{ user.middle_name }}</td>
                        <td class="active" align="center">@{{ user.progress[0] ? user.progress[0].grade: '-' }}</td>
                    </tr>
                </tbody>
            </table>
            <div class="empty" :class="{ loading }" v-else>
                <div class="empty-icon">
                    <i class="fa fa-list-alt fa-5x" aria-hidden="true"></i>
                </div>
                <h5 class="empty-title">Чтобы открыть ведомость группы, необходимо выбрать необходимую группу и дисциплину.</h5>
                <div class="empty-action">
                    <div class="columns">
                        <div class="column col-6 text-right">
                            <select class="form-select" v-model="search.group">
                                <option value="" disabled>Выберите группу</option>
                                @foreach($groups as $group)
                                <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="column col-6 text-left">
                            <select class="form-select" v-model="search.subject">
                                <option value="" disabled>Выберите дисциплину</option>
                                @foreach($subjects as $subject)
                                <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </page-statement>
@endsection