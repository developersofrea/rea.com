@extends('main.layouts.base')

@section('title', 'Создать нового пользователя')

@section('content')
    <user-create inline-template>
        <div class="columns create">
            <div class="column col-8 col-sm-12">
                <div class="toast toast-danger" v-if="request.errors.length">
                    <i class="icon icon-error_outline"></i>
                    <ul>
                        <li v-for="error of request.errors">@{{ error }}</li>
                    </ul>
                </div>
                <div class="toast toast-success" v-if="request.success">
                    Аккаунт был успешно создан.
                </div>
                <form>
                    <div class="columns">
                        <div class="column col-3 col-sm-12">
                            <div class="form-group">
                                <label class="form-label">Имя</label>
                                <input class="form-input" type="text" v-model="form.first_name.value" placeholder="Иван" />
                            </div>
                        </div>
                        <div class="column col-5 col-sm-12">
                            <div class="form-group">
                                <label class="form-label">Фамилия</label>
                                <input class="form-input" type="text" v-model="form.last_name.value" placeholder="Иванов" />
                            </div>
                        </div>
                        <div class="column col-4 col-sm-12">
                            <div class="form-group">
                                <label class="form-label">Отчество</label>
                                <input class="form-input" type="text" v-model="form.middle_name.value" placeholder="Иванович" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Логин</label>
                        <input class="form-input" type="text" v-model="form.username.value" placeholder="username" />
                    </div>
                    <div class="columns">
                        <div class="column col-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label">Пароль</label>
                                <input class="form-input" type="text" v-model="form.password.value" placeholder="Пароль" />
                            </div>
                        </div>
                        <div class="column col-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label">Подтверждение</label>
                                <input class="form-input" type="text" v-model="form.password_confirmation.value" placeholder="Повторите пароль" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Пол</label>
                        <label class="form-radio">
                            <input type="radio" name="gender" v-model="form.gender.value" value="1" />
                            <i class="form-icon"></i> Мужской
                        </label>
                        <label class="form-radio">
                            <input type="radio" name="gender" v-model="form.gender.value" value="2" />
                            <i class="form-icon"></i> Женский
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Email</label>
                        <input class="form-input" type="text" v-model="form.email.value" placeholder="ivanivanov@mail.ru" />
                    </div>
                    <div class="form-group">
                        <label class="form-label">Телефон</label>
                        <input class="form-input" type="text" v-model="form.phone.value" placeholder="+7xxxxxxxxxx" />
                    </div>
                    <div class="form-group">
                        <label class="form-label">День рождения</label>
                        <input class="form-input" type="text" v-model="form.birthday.value" placeholder="1997-12-01" />
                    </div>
                    <div class="form-group">
                        <label class="form-label">Тип пользователя</label>
                        <label class="form-radio">
                            <input type="radio" name="type" v-model="form.type.value" value="student" />
                            <i class="form-icon"></i> Студент
                        </label>
                        <label class="form-radio">
                            <input type="radio" name="type" v-model="form.type.value" value="teacher" />
                            <i class="form-icon"></i> Преподаватель
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Работает</label>
                        <input class="form-input" type="text" v-model="form.place_of_work.value" placeholder="РЭУ" />
                    </div>
                    <div class="form-group">
                        <label class="form-label">Место проживание</label>
                        <input class="form-input" type="text" v-model="form.residence.value" placeholder="Кемерово, Россия" />
                    </div>
                    <div class="form-group">
                        <select class="form-select" v-model="form.group_id.value">
                            <option disabled value="">Выберите группу</option>
                            <option value="0">Преподаватель</option>
                            @foreach($groups as $group)
                                <option value="{{ $group->id }}">{{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="button" class="btn btn-primary" @click="apply">
                        Создать
                    </button>
                </form>
            </div>
            <div class="column col-4 col-sm-12">
                <div class="form-group pt-10">
                    <label class="form-label">Фотография</label>
                    <button type="button" class="btn btn-block btn-primary btn-lg centered btn-file" :class="{ loading: cropping.loading }">
                        Загрузить фотографию <input v-on:change="imageUpload" type="file" name="file_attach[]" accept="image/*">
                    </button>
                </div>
                <div class="centered img-responsive" style="overflow:hidden;margin-left:5px;" @click="cropping.modal = true">
                    <img v-if="cropping.image.url" :src="cropping.image.url" alt="Profile picture" v-previewer="previewer">
                </div>
            </div>
            <support-modal :active="cropping.modal" @close="closeCropModal">
                <div slot="header" class="modal-title">
                    Обрезать фотографию
                </div>
                <div slot="content">
                    <img v-cropper="cropping.image" :src="cropping.image.url" class="img-responsive cropping image centered" />
                </div>
                <div slot="footer">
                    <button type="button" class="btn btn-primary" @click="closeCropModal">
                        Обрезать
                    </button>
                </div>
            </support-modal>
        </div>
    </user-create>
@endsection