@extends('main.layouts.base')

@section('title', 'Страница пользователя')

@section('content')
    <div class="panel profile" v-tabs>
        <div class="panel-header">
            <div class="columns">
                <div class="column col-3 col-sm-12">
                    <img src="{{ $profile->avatar }}" class="centered" alt="..." />
                </div>
                <div class="column col-6 col-sm-12">
                    <h4>{{ $profile->last_name }}  {{ $profile->first_name }} {{ $profile->middle_name }}</h4>
                    <h6>{{ $profile->residence }}</h6>
                    <h6>{{ $profile->phone }}</h6>
                    <h6>{{ $profile->email }}</h6>
                </div>
                <div class="column col-3 col-sm-12">
                    @if(Auth::user()->hasRole('teacher') || Auth::id() == $profile->id)
                    <a class="btn btn-primary btn-lg centered" href="/user/{{ $profile->id }}/edit">Редактировать</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="panel-nav">
            <ul class="tab tab-block">
                <li class="tab-item active" data-tab="contacts">
                    <a>Контактная информация</a>
                </li>
                <li class="tab-item" data-tab="progress">
                    <a>Оценки по предметам</a>
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="columns tab-content" id="contacts">
                @if($profile->email)
                <div class="column col-4 col-sm-12 cell">
                    <div class="icon">
                        <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
                    </div>
                    {{ $profile->email }}
                </div>
                @endif
                @if($profile->phone)
                <div class="column col-4 col-sm-12 cell">
                    <div class="icon">
                        <i class="fa fa-phone fa-3x" aria-hidden="true"></i>
                    </div>
                    {{ $profile->phone }}
                </div>
                @endif
                @if($profile->place_of_work)
                <div class="column col-4 col-sm-12 cell">
                    <div class="icon">
                        <i class="fa fa-briefcase fa-3x" aria-hidden="true"></i>
                    </div>
                    Работает в {{ $profile->place_of_work }}
                </div>
                @endif
                @if($profile->group)
                <div class="column col-4 col-sm-12 cell">
                    <div class="icon">
                        <i class="fa fa-graduation-cap fa-3x" aria-hidden="true"></i>
                    </div>
                    Группа {{ $profile->group->name }}
                </div>
                @endif
                @if($profile->residence)
                <div class="column col-4 col-sm-12 cell">
                    <div class="icon">
                        <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
                    </div>
                    {{ $profile->residence }}
                </div>
                @endif
                @if($profile->birthday)
                <div class="column col-4 col-sm-12 cell">
                    <div class="icon">
                        <i class="fa fa-birthday-cake fa-3x" aria-hidden="true"></i>
                    </div>
                    {{ $profile->birthday }}
                </div>
                @endif
            </div>
            <div class="{{ !count($profile->progress) ? 'empty': 'pt-10' }} tab-content" id="progress" style="display: none;">
                @if(! count($profile->progress))
                <div class="empty-icon">
                    <i class="icon icon-people"></i>
                </div>
                <h4 class="empty-title">У этого студента ещё нет оценок</h4>
                @if(Auth::user()->hasRole('teacher'))
                <p class="empty-subtitle">Откройте менеджер оценок для управления академической успеваемости студента.</p>
                <div class="empty-action">
                    <a href="/user/{{ $profile->id }}/progress" class="btn btn-primary">Менеджер оценок</a>
                </div>
                @endif
                @else
                @if(Auth::user()->hasRole('teacher'))
                <div class="text-right">
                    <a href="/user/{{ $profile->id }}/progress" class="btn btn-link">Менеджер оценок</a>
                </div>
                <div class="divider"></div>
                @endif
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2">Предмет</th>
                            <td colspan="8">Семестр:</td>
                            <th class="rotate deg90 no-margin no-border active" rowspan="2"><div><span>Итоговая оценка</span></div></th>
                        </tr>
                        <tr>
                            @for ($i = 1; $i <= 8; $i++)
                            <th>{{ $i }}-й</th>
                            @endfor
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($profile->progress as $base) 
                        <tr>
                            <td>{{ $base->subject->name }}</td>
                            @for ($i = 1; $i < 9; $i++)
                                <td>
                                    {{  $base->{$i . '_sem'} ? : '-' }}
                                </td>
                            @endfor
                            <td>
                               {{  $base->grade }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
@endsection