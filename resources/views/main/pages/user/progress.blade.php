@extends('main.layouts.base')

@section('title', 'Оценки ученика')

@section('content')
    <user-progress :user_id="{{ $profile->id }}" :progress="{{ $profile->progress->toJson() }}" :subjects="{{ $subjects }}" inline-template>
    <div class="search">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th rowspan="2">Предмет</th>
                    <td colspan="8">Семестр:</td>
                    <th class="rotate deg90 no-border active" rowspan="2"><div><span>Итоговая оценка</span></div></th>
                </tr>
                <tr>
                    @for ($i = 1; $i <= 8; $i++)
                    <th>{{ $i }}-й</th>
                    @endfor
                </tr>
            </thead>
            <tbody>
                <tr v-for="subject in table">
                    <td>@{{ _.find(subjects, [ 'id', subject.subject_id]).name }}</td>
                    <td v-for="i in 8">
                        <select class="form-select" v-model="subject[i + '_sem']" style="min-width: 0px">
                            <option value="">-</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </td>
                    <td class="active">
                        <select class="form-select" v-model="subject.grade" style="min-width: 0px">
                            <option value="">-</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="form-group float-left mt-10">
            <select class="form-select" v-model="creator.subject">
                <option value="" disabled>Выберите предмет</option>
                <option v-for="subject in subjects" :value="subject.id">@{{ subject.name }}</option>
            </select>
            <button class="btn btn-primary" @click="createTable">Добавить</button>
        </div>
        <button class="btn btn-primary float-right mt-10" :class="{ loading: loading }" @click="save">Сохранить</button>
    </div>
    </user-progress>
@endsection