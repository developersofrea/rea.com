@extends('main.layouts.base')

@section('title', 'Электронный журнал')

@section('content')
    <page-diary :group="'{{ $group }}'" inline-template>
        <div>
            <div class="columns">
                <div class="column col-9"></div>
                <div class="column col-3">
                    <div class="form-group">
                        <label class="form-label" for="input-group-1">Группа</label>
                        <select class="form-select" v-model="search.group">
                            <option value="" disabled="">Выберите группу</option>
                            @foreach($groups as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="divider pt-10"></div>
            <table class="table table-hover table-header-rotated" v-if="data" :class="{ loading }">
                <thead>
                    <tr>
                        <th></th>
                        <th class="rotate" v-for="subject in data.subjects"><div><span>@{{ subject }}</span></div></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="user in data.result.users">
                        <td>@{{ user.last_name }} @{{ user.first_name[0] }}.@{{ user.middle_name[0] }}.</td>
                        <td v-for="subject in data.subjects">
                            <template v-for="progress in user.progress" v-if="subject == progress.subject.name">
                                @{{ progress.grade }}
                            </template>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="empty" :class="{ loading }" v-else>
                <div class="empty-icon">
                    <i class="fa fa-list-alt fa-5x" aria-hidden="true"></i>
                </div>
                <h5 class="empty-title" v-if="search.group">
                    Похоже, что студенты в данной группе ещё не получали никаких оценок.
                    <p>Выбрать другую группу?</p>
                </h5>
                <h5 class="empty-title" v-else>Чтобы открыть электронный журнал группы, необходимо выбрать необходимую группу.</h5>
                <div class="empty-action">
                    <select class="form-select" v-model="search.group">
                        <option value="" disabled="">Выберите группу</option>
                        @foreach($groups as $group)
                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </page-diary>
@endsection