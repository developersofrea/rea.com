<div class="main-user">
	<div class="main-user-left">
		<a href="/user/{{ Auth::id() }}">
		<figure class="avatar avatar-lg" data-initial="{{ mb_substr(Auth::user()->first_name, 0, 1) . mb_substr(Auth::user()->last_name, 0, 1) }}">
			@if (Auth::user()->avatar)
			<img src="{{ Auth::user()->avatar }}" />
			@endif
		</figure>
		</a>
	</div>
	<div class="main-user-right">
		<p class="main-user-right-name">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
		<p><a href="/user/{{ Auth::id() }}/edit">Редактировать</a></p>
	</div>
</div>