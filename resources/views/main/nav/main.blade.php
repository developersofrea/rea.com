<ul class="nav main-menu">
	<li {{ Request::is('/') ? 'class=active': '' }}><a href="/">Главная</a></li>
	@if(Auth::user()->hasRole('teacher'))
	<li {{ Request::is('group/manager') ? 'class=active': '' }}><a href="/group/manager">Менеджер групп</a></li>
	<li {{ Request::is('subject/manager') ? 'class=active': '' }}><a href="/subject/manager">Менеджер предметов</a></li>
	@endif
</ul>

<ul class="main-menu">
	@if(Auth::user()->hasRole('teacher'))
	<li {{ Request::is('user/create') ? 'class=active': '' }}><a href="/user/create">Создать пользователя</a></li>
	<li {{ Request::is('main/diary') ? 'class=active': '' }}><a href="/main/diary">Электронный журнал</a></li>
	@endif
	<li {{ Request::is('user/search') ? 'class=active': '' }}><a href="/user/search">Поиск пользователей</a></li>
</ul>

<ul class="main-menu">
	<li {{ Request::is('user/' . Auth::id()) ? 'class=active': '' }}><a href="/user/{{ Auth::id() }}">Мой профиль</a></li>
	<li {{ Request::is('user/' . Auth::id() . '/edit') ? 'class=active': '' }}><a href="/user/{{ Auth::id() }}/edit">Редактировать мой профиль</a></li>
	<li><a href="/auth/logout">Выйти</a></li>
</ul>