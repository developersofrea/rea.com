@extends('main.layouts.base')

@section('title', 'Чат ' . $chat->name)

@section('content')
    <chat inline-template>
        <div class="panel">
            <div class="panel-header">
                <div class="panel-title"><h5>Чат {{ $chat->name }}</h5></div>
            </div>
            <div class="panel-body">
                <div class="tile">
                    <div class="tile-icon">
                        <figure class="avatar">
                            <img src="img/avatar-1.png">
                        </figure>
                    </div>
                    <div class="tile-content">
                        <p class="tile-title">Thor Odinson</p>
                        <p class="tile-subtitle">Earth's Mightiest Heroes joined forces to take on threats that were too big for any one hero to tackle...</p>
                        <div class="tile-action">
                            <div class="tile tile-centered">
                                <div class="tile-icon">
                                    <div class="tile-icon">
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="tile-content">
                                    <div class="tile-title">spectre-docs.pdf</div>
                                    <div class="tile-subtitle">14MB · 1 Jan, 2017</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="input-group">
                    <input class="form-input" placeholder="Введите сообщение" type="text">
                    <button class="btn btn-primary input-group-btn">Отправить</button>
                </div>
            </div>
        </div>
    </chat>
@endsection