<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<head>
		<meta charset="utf-8">
		<link href="/css/spectre.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
		<link href="/css/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<link href="/css/jquery.Jcrop.min.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<!--[if IE]>
			<link href="/css/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<![endif]-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>
</head>
<body>
	<div class="container" id="app">
		<div class="columns main">
			<div class="column col-3 col-sm-12 main-left">
				<!-- user start -->
				@include('main.nav.user')
				<!-- menu start -->
				@include('main.nav.main')
				<footer class="hide-sm">
					Российский экономический университет им. Г.В. Плеханова
				</footer>
			</div>
			<div class="column col-9 col-sm-12 main-right">
				<p class="header">@yield('title')</p>
				@yield('content')
			</div>
		</div>
	</div>
	<script>
		var Application = {!! json_encode([
	        'csrfToken' => csrf_token(),
	    ]) !!};
	</script>
	<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>