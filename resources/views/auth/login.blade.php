@extends('auth.base')

@section('title', 'Авторизация')

@section('content')
<form method="post" action="/auth/login">
	{{ csrf_field() }}
	<div class="form-group">
		<label class="form-label" for="input-name">Логин</label>
		<input name="username" class="form-input" type="text" id="input-name" placeholder="Логин" />
	</div>
	<div class="form-group">
		<label class="form-label" for="input-password">Пароль</label>
		<input name="password" class="form-input" type="password" id="input-password" placeholder="Пароль" />
	</div>
	<div class="form-group">
		<label class="form-checkbox">
			<input type="checkbox" name="remember">
			<i class="form-icon"></i> Запомнить меня
		</label>
	</div>
	<div class="form-group">
		<button class="btn btn-primary btn-login-action" type="submit">Войти</button>
	</div>
</form>
@endsection