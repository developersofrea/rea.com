<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<link href="/css/spectre.css" media="screen, projection" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
	<link href="/css/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
	<!--[if IE]>
		<link href="/css/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
	<![endif]-->
</head>
<body>
	<div class="login">
		<div class="background"></div>
		<div class="container">
			<div class="columns">
				<div class="column col-4 col-xs-12 box">
					<p class="header centered">
						@yield('title')
					</p>
					@if (count($errors) > 0)
						<div class="form-group">
							<div class="toast toast-danger">
								<ul>
									@foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
								</ul>
							</div>
						</div>
					@endif
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	<script src="/js/app.js"></script>
</body>
</html>