
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('user-create', require('./components/user/create.vue'));
Vue.component('user-edit', require('./components/user/edit.vue'));
Vue.component('user-progress', require('./components/user/progress.vue'));
Vue.component('page-search', require('./components/search').default);
Vue.component('page-diary', require('./components/diary'));
Vue.component('page-statement', require('./components/statement'));
Vue.component('group-manager', require('./components/group/manager').default);
Vue.component('subject-manager', require('./components/subject/manager'));
Vue.component('chat', require('./components/chat.vue').default);

Vue.component('support-modal', require('./components/support/modal.vue'));

Vue.directive('tabs', require('./directives/tabs.js').default);

const app = new Vue({
    el: '#app'
});
