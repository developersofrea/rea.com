import Form from 'app/support/form/form'
import Input from 'app/support/form/input'

export default {
	props: ['groups'],

	data() {
		return {
			form: new Form({
				name: new Input()
			}),
			request: {
				success: false,
				loading: false,
				errors: []
			},
			data: this.groups
		}
	},

	methods: {
		create () {
			this.request.loading = true;
			axios.post('/api/v1/group/create', this.form.all())
				 .then(response => {
				 	this.request.loading = false;
				 	this.request.success = response.data.success;
				 	if(! response.data.success) {
				 		return alert(response.data.errors);
				 	}
				 	this.data.push(response.data.data);
				 });
		},

		remove (event, id) {
			$(event.target).addClass('loading');
			axios.post('/api/v1/group/' + id + '/delete')
				 .then(response => {
				 	if(response.data.success) {
				 		return this.data = this.data.filter((element) => element.id != id);
				 	}
				 	alert(response.data.data);				 	
				 });
		}
	}
}