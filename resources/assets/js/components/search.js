import Form from 'app/support/form/form'
import Input from 'app/support/form/input'

export default {
	props: ['database', 'request', 'city', 'gender', 'age_from', 'age_to'],

	data() {
		return {
			form: new Form({
				request: new Input(this.request),
				city: new Input(this.city),
				gender: new Input(this.gender),
				age_from: new Input(this.age_from),
				age_to: new Input(this.age_to),
				page: new Input(this.database.current_page)
			}),
			users: this.database,
			query: {
				success: false,
				loading: 0,
				errors: []
			}
		};
	},
	watch: {
		['form.request.value']: function () {
			return this.make();
		},
		['form.city.value']: function () {
			return this.make();
		},
		['form.gender.value']: function () {
			return this.make();
		},
		['form.gender.value']: function () {
			return this.make();
		},
		['form.age_from.value']: function () {
			return this.make();
		},
		['form.age_to.value']: function () {
			return this.make();
		}
	},
	methods: {
		make: _.debounce(
			function () {
				this.search()
			},
			500
		),
		search () {
			this.query.loading = true;
			this.changeUrl(this.form.all());
			axios.post('/api/v1/user/search', this.form.all())
				 .then(response => {
				 	this.query.loading = false;
				 	this.query.success = response.data.success;
				 	if(! response.data.success) {
				 		return this.query.errors = response.data.data;
				 	}
				 	this.users = response.data.data;
				 });
		},
		openPage (page) {
			this.form.page.value = page;
			this.search();
		},
		nextPage () {
			this.form.page.value = this.users.current_page + 1;
			this.search();
		},
		prevPage () {
			this.form.page.value = this.users.current_page - 1;
			this.search();
		},
		changeUrl () {
			window.history.pushState(this.form, 'searching...', this.convertToUrlString(this.form.all()))
		},
		convertToUrlString (obj) {
			return '?' + Object.keys(obj).map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(obj[key])).join('&');
		}
	}
}