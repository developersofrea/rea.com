export default {
	bind($el, binding) {
        setTimeout(() =>{
            binding.def.update($el, binding);
        }, 0);
    },

    update($el, binding) {
        let value = JSON.parse(binding.value);
        let parent_w = $($el).parent().width();
        let multiplier = parent_w / value.w;
        $($el).parent().css({ height: parent_w });
        return binding.def.getMeta(value.url)
            .then((data) => {
                return $($el).css(
                    binding.def.getStyles(data.width, data.height, value.x, value.y, multiplier)
                );
            });
    },

    getStyles(width, height, x, y, multiplier) {
        return {
            width: Math.round(multiplier * width) + 'px',
            height: Math.round(multiplier * height) + 'px',
            marginLeft: '-' + Math.round(multiplier * x) + 'px',
            marginTop: '-' + Math.round(multiplier * y)+ 'px',
            display: 'inline'
        }
    },

    getMeta(url) {
        return new Promise((resolve, reject) => {
            this.createImage(url, function () {
                resolve(this);
            });
        });
    },

    createImage(url, callback) {
        var img = new Image();
        img.onload = callback;
        img.src = url;
    }
}