export default {
	bind ($el, binding) {
		$($el).find('ul.tab-block li').click(function () {
			$($el).find('ul.tab-block li').removeClass('active');
			$($el).find('.tab-content').hide();
			$(this).addClass('active');
			$($el).find('#' + $(this).attr('data-tab')).show();
		});
	},
}