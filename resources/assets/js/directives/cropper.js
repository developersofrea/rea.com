export default {
	image: {},
	el_api: null,
	real_data: {},
	multiplier: 1,

	bind($el, binding) {
		binding.def.image = binding.value;
		binding.def.update($el, binding);
	},

	update($el, binding) {
		if(binding.def.changed(binding)) {
			return true;
		}
		binding.def.destroy();
		binding.def.start($el, binding);
	},

	start($el, binding) {
		binding.def.getMeta(binding.value.url)
			.then(data => {
				binding.def.multiplier = data.height / $($el).parent().height();
				return binding.def.changeSize($el, data);
			})
			.then(() => {
				return binding.def.crop($el, binding);
			})
	},
	// Change size of the image
	changeSize($el, data) {
		return $($el).css({
			width: data.width / this.multiplier,
			height: data.height / this.multiplier
		});
	},
	// to crop image
	crop($el, binding) {
		$($el).Jcrop({
			aspectRatio: 1,
            onChange: values => binding.def.changeCoords(values, binding),
            onSelect: values => binding.def.changeCoords(values, binding)
		}, function () {
			binding.def.el_api = this;
		});
	},
	// Change coords of cropping the image.
	changeCoords(values, binding) {
        binding.def.image.x = values.x * binding.def.multiplier;
        binding.def.image.y = values.y * binding.def.multiplier;
        binding.def.image.w = values.w * binding.def.multiplier;
        binding.def.image.h = values.h * binding.def.multiplier;
    },
    // Get meta information about the image
    getMeta(url) {
        return new Promise((resolve, reject) => {
            this.createImage(url, function () {
                resolve({
                    width: this.width,
                    height: this.height
                });
            })
        });
    },
    // Check if the image has been changed
	changed(binding) {
		return binding.value.url == null || binding.oldValue && binding.oldValue.url == binding.value.url;
	},
	// Destroying the image
	destroy() {
        return this.isEnable()? this.el_api.destroy(): false;
    },
    // Check if the image exists
    isEnable() {
        return this.el_api != null;
    },
    // Creating js image
    createImage(url, callback) {
        var img = new Image();
        img.onload = callback;
        img.src = url;
    }
}