class Input {
	constructor (value) {
		this.value = value;
		this.status = null;
		this.errors = [];
	}

	setErrors (errors) {
		this.errors = errors.map(value => value);
	}
}

export default Input;