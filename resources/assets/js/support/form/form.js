class Form {
	constructor (elements) {
		this._elements = {};
		this.pushArray(elements);
	}

	pushArray (elements) {
		_.forIn(elements, (value, key) => this.push(key, value));
		return this;
	}

	push (name, element) {
		this[name] = this._elements[name] = element;
		return this;
	}

	all () {
		return _.mapValues(this._elements, (element) => element.value);
	}

	setErrors (errors) {
		_.forIn(errors, (value, key) => {
			this._elements[key].setErrors(value);
		});
		return this;
	}
}

export default Form;