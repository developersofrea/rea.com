export default class Errors {
	constructor () {
		this._errors = {};
	}

	get all () {
		return this._errors;
	}

	set (input, errors) {
		this._errors[input] = errors;
	}

	setArray (errors) {
		_.forIn(errors, (value, key) => {
			this.set(key, value);
		});
	}

	reset () {
		this._errors = {};
	}

	have () {
		return Object.keys(this._errors).length > 0;
	}
}