class Uploader {
	constructor () {
		this.allowed_file_size = 1048576;
        this.allowed_files = ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg'];
	}

	upload (el) {
		if(! this.validate(el)) {
            return this.responseError('The file is unsupported file type or make sure total file size is less than 1 MB!');
        }
        return Promise.resolve().then(() => {
            return this.ajax(el);
        });
	}

	ajax (el) {
		return $.ajax({
            url: '/api/v1/image/upload',
            type: 'post',
            data: this.getFormData(el),
            headers: {
                'X-CSRF-TOKEN': Application.csrfToken
            },
            dataType: "json",
            contentType: false,
            cache: false,
            processData:false
        });
	}

	validate(element) {
        var proceed = true;
        if(window.File && window.FileReader && window.FileList && window.Blob) {
            var total_files_size = 0;
            $(element.files).each((i, ifile) => {
                if(this.allowed_files.indexOf(ifile.type) === -1) {
                    proceed = false;
                }
                total_files_size += ifile.size;
            });
            proceed = (total_files_size >= this.allowed_file_size || proceed == false) ? false: true;
        }
        return proceed;
    }

    getFormData(el) {
        var formData = new FormData();
        formData.append("image", $(el)[0].files[0]);
        return formData;
    }

	responseError(err) {
		return Promise.reject().then(() => ({
            success: false,
            error: err
        }));
	}
}

export default new Uploader;