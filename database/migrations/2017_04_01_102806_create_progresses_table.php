<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject_id');
            $table->integer('student_id');
            $table->integer('1_sem')->nullable();
            $table->integer('2_sem')->nullable();
            $table->integer('3_sem')->nullable();
            $table->integer('4_sem')->nullable();
            $table->integer('5_sem')->nullable();
            $table->integer('6_sem')->nullable();
            $table->integer('7_sem')->nullable();
            $table->integer('8_sem')->nullable();
            $table->integer('grade')->nullable();
            $table->date('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progresses');
    }
}
